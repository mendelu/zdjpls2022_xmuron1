"""
příklad: Načtěte od uživatele vstup a vypište
jen jeho poslední polovinu
napr. abcd -> cd
"""
vstup = input()
vystup = vstup[len(vstup)//2:]
print(vystup)
