"""
Vytvořte program, který bude generovat
jména týmů na základe jmen členů týmu.
Jméno týmu se vytvoří spojením prvních dvou
písmen členu týmu.
"""
clenove = ['tom','pavel','franta']
# -> topafr
jmeno_teamu = ""
for jmeno in clenove:
    jmeno_teamu += jmeno[:2]
print(jmeno_teamu)