"""
Pomocí for cyklů vypište následující
*
* *
* * *
* * * *
* * * * *
* * * * * *
* * * * * * *
* * * * * * * *
* * * * * * *
* * * * * *
* * * * *
* * * *
* * *
* *
*
"""

for i in range(1,10):
    print(i*" * ")
for i in range(8,0,-1):
    print(i * " * ")
