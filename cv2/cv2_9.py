"""
Mějme knihovny a jejich knihy uložené v setu.
mzk = {'Harry Potter',
'Dvacet tisíc mil pod mořem',
'Hoši od Bobří řeky'}
mahenova = {'Harry Potter', 'Babička', 'Macbeth'}

Ukol 1
Zjistěte, které tituly se nachází v depozitáři
obou dvou knihoven
Ukol 2
Získejte set titulů, pokud by došlo ke sloučení knihoven
Ukol 3
Zjistěte, které knihy jsou v MZK
a nejsou v Mahenově knihovně
"""