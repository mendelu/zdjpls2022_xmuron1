"""
Aproximujte hodnotu čísla pi pomocí
následujícího nekonečného součtu
"""
sum = 0
for i in range(1,1000):
    sum += 1/(i**2)

pi = (sum*6)**(1/2)
print(pi)