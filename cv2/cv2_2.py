"""
Napište program,
který v zadaném stringu zamění všechny
výskyty prvního znaku za $,
první znak však zůstane zachován
např.
restart -> resta$t
pepa -> pe$a
pepaa -> pe$aa
abca -> abc$
"""
r = "restart"
r = r[0] + r[1:].replace(r[0],"$")
print(r)