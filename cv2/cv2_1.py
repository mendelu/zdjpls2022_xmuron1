"""
Vygenerujte pomocí operátorů nad
stringy poslední sloku
písničky Justin Biebera: Boyfriend, která zní:

na na na, na na na, na na na ey
na na na, na na na, na na na ey
na na na, na na na, na na na ey
na na na, na na na, na na na ey
"""
text = (((2*"na "+"na, ")*2)+(3*"na ")+"ey\n")*4
print(text)
