"""
Vytvořte program, jenž umožní zjistit,
zda uživatel s něčím souhlasí čí nikoliv.
Načtěte od uživatele text a porovnejte
ho se slovy souhlasu či nesouhlasu.
Uživatel může projevit souhlas pomocí
slov jako např. “souhlasím“, “ano“, “ok“.
Nesouhlas pak slovy jako např.
“ne“, “nesouhlasím“.

Vypište rozhodnutí uživatele na obrazovku.
Použijte operátor in
"""